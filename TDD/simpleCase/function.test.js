const sum = require('./functions');

test('1 + 2 = 3', () => {
    expect(sum(1,2)).toBe(3);
    
});

test('2 + 2 = 4', () => {
    expect(2 + 2).toBe(4);
});

test('5 + 5 = 10', () => {
    expect(5+5).toBe(10);
});

test('contain word', () => {
    const word = 'mujer';
    words = ['hombre','mujer','niña','adolescente','casa'];
    expect(words).toContain(word)
});