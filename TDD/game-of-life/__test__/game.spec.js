require("../game");
const {
  isAlive,
  generate,
  regenerate,
  countNeighbours,
  drawGrid,
  attachGridEventHandler,
  getCellsFromDom,
  start
} = window.game;

jest.useFakeTimers();

describe("game of life", () => {
  describe("isAlive algorithm", () => {
    test("dead cell with no neighbours", () => {
      expect(isAlive(0, 0)).toEqual(0);
    });
    test("dead cell with 3 neighbours should return 1", () => {
      expect(isAlive(0, 3)).toEqual(1);
    });
    test("live cell with no neighbours should return 0", () => {
      expect(isAlive(1, 0)).toEqual(0);
    });
    test("live cell with 2 neighbours should return 1", () => {
      expect(isAlive(1, 2)).toEqual(1);
    });
  });

  describe("generate function", () => {
    test("should create an array of x * x", () => {
      expect(generate(1)).toEqual([0]);
      expect(generate(2)).toEqual([0, 0, 0, 0]);
    });
  });

  describe("countNeighbours", () => {
    test("should count 0 for array of one", () => {
      expect(countNeighbours([1], 0)).toBe(0);
    });
    test("should count 2 neighbours alive", () => {
      expect(countNeighbours([1, 1, 1, 0], 0)).toBe(2);
    });
    test("should count 2 neighbours alive", () => {
      expect(countNeighbours([1, 1, 1, 0], 1)).toBe(2);
    });
    test("should count 2 neighbours alive", () => {
      expect(countNeighbours([1, 1, 1, 0], 2)).toBe(2);
    });
    test("should count 3 neighbours alive", () => {
      expect(countNeighbours([1, 1, 1, 0], 3)).toBe(3);
    });
    test("should count 3 neighbours alive", () => {
      expect(countNeighbours([1, 1, 1, 0, 0, 0, 0, 0, 0], 4)).toBe(3);
    });
  });

  describe("regenerate function", () => {
    test("should  not update dead cells", () => {
      const cells = generate(1);
      expect(regenerate(cells)).toEqual(cells);
    });
    test("should kill live cell if all neighbours are dead", () => {
      const cells = generate(2);
      cells[0] = 1;
      expect(regenerate(cells)).toEqual(generate(2));
    });
    test("should return all live cells", () => {
      const cells = [1, 1, 1, 0];
      const allLiveCells = [1, 1, 1, 1];
      expect(regenerate(cells)).toEqual(allLiveCells);
    });
  });

  describe("browser grid", () => {
    test("should display 1 dead cell", () => {
      document.body.innerHTML = '<div id="grid"></div>';
      drawGrid([0]);
      expect(document.querySelectorAll(".container").length).toBe(1);
      expect(document.querySelectorAll(".cell").length).toBe(1);
      expect(document.querySelectorAll(".dead").length).toBe(1);
    });
    test("should display 1 alive cell", () => {
      document.body.innerHTML = '<div id="grid"></div>';
      drawGrid([1]);
      expect(document.querySelectorAll(".container").length).toBe(1);
      expect(document.querySelectorAll(".cell").length).toBe(1);
      expect(document.querySelectorAll(".alive").length).toBe(1);
    });
    test("should display 4 cells", () => {
      document.body.innerHTML = '<div id="grid"></div>';
      drawGrid([0, 0, 1, 1]);
      expect(document.querySelectorAll(".cell").length).toBe(4);
      expect(document.querySelectorAll(".alive").length).toBe(2);
      expect(document.querySelectorAll(".dead").length).toBe(2);
      drawGrid([1, 1, 0, 0]);
      expect(document.querySelectorAll(".cell").length).toBe(4);
      expect(document.querySelectorAll(".alive").length).toBe(2);
      expect(document.querySelectorAll(".dead").length).toBe(2);
    });
  });

  describe("event handler for grid", () => {
    test("click on cells should toggle live/dead", () => {
      document.body.innerHTML = '<div id="grid"></div>';
      drawGrid([0]);
      attachGridEventHandler();
      expect(document.querySelectorAll(".dead").length).toEqual(1);
      expect(document.querySelectorAll(".alive").length).toEqual(0);
      const cell = document.querySelectorAll(".dead")[0];
      cell.click();
      expect(document.querySelectorAll(".dead").length).toEqual(0);
      expect(document.querySelectorAll(".alive").length).toEqual(1);
      cell.click();
      expect(document.querySelectorAll(".dead").length).toEqual(1);
      expect(document.querySelectorAll(".alive").length).toEqual(0);
    });
  });

  describe('get cells from dom', () => {
      test('should get living and dead cells from dom', () => {
        document.body.innerHTML = '<div id="grid"></div>';
        const cells = [0, 0, 1, 1]
        drawGrid(cells);
        expect(getCellsFromDom()).toEqual(cells);
      });
  });

  describe('start function', () => {
    document.body.innerHTML = '<div id="grid"></div>';
    const getCellsFromDomSpy = jest.spyOn(game, 'getCellsFromDom');  
    const regenerateSpy = jest.spyOn(game, 'regenerate');
    const drawGridSpy = jest.spyOn(game, 'drawGrid');
    game.start();
    jest.runOnlyPendingTimers();
    expect(setInterval).toHaveBeenCalled();
    expect(getCellsFromDomSpy).toHaveBeenCalled();
    expect(regenerateSpy).toHaveBeenCalled();
    expect(drawGridSpy).toHaveBeenCalled();
  });

  describe('stop function', () => {
      test('should clear interval', () => {
          game.stop();
          expect(clearInterval).toHaveBeenCalled();
          
      });
      
  });
});
