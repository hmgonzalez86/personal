const isAlive = (cell, neighbours) =>
  neighbours === 3 || (Boolean(cell) && neighbours === 2) ? 1 : 0;

const generate = (row) => new Array(row * row).fill(0);

const add = (...args) => args.reduce((acc, next) => acc + (next || 0), 0);

const leftColumnValues = (idx, width, cells) =>
  idx % width
    ? [cells[idx - width - 1], cells[idx - 1], cells[idx + width - 1]]
    : [];

const rightColumnValues = (idx, width, cells) =>
  (idx + 1) % width
    ? [cells[idx - width + 1], cells[idx + 1], cells[idx + width + 1]]
    : [];

const countNeighbours = (cells, idx) => {
  const width = Math.sqrt(cells.length);
  return add(
    cells[idx - width],
    cells[idx + width],
    ...leftColumnValues(idx, width, cells),
    ...rightColumnValues(idx, width, cells)
  );
};

const regenerate = (cells) =>
  cells.map((cell, index) => isAlive(cell, countNeighbours(cells, index)));

const createElement = (className) => {
  const element = document.createElement("div");
  element.className = className;
  return element;
};

const drawGrid = (cells) => {
  const width = Math.sqrt(cells.length);
  const grid = document.getElementById("grid");
  const container = createElement("container");
  let row;
  cells.forEach((cell, index) => {
    if (index % width === 0) {
      row = createElement("row");
      container.appendChild(row);
    }
    const cellEl = createElement(`cell ${cell === 0 ? "dead" : "alive"}`);
    row.appendChild(cellEl);
  });
  grid.innerHTML = "";
  grid.appendChild(container);
};

const attachGridEventHandler = () => {
  document.getElementById("grid").addEventListener("click", (event) => {
    const className = event.target.className;
    event.target.className = className.includes("dead")
      ? className.replace("dead", "alive")
      : className.replace("alive", "dead");
  });
};

const getCellsFromDom = () =>
    Array.from(document.querySelectorAll('.cell'))
    .map(cell => cell.className.includes('dead') ? 0 : 1);

let gameLoop;

const start = () => {
    let generation = game.getCellsFromDom();
    gameLoop = setInterval(() => {
        generation = game.regenerate(generation);
        game.drawGrid(generation);
    },500)
}

const stop = () => clearInterval(gameLoop);


window.game = {
  isAlive,
  generate,
  regenerate,
  countNeighbours,
  drawGrid,
  attachGridEventHandler,
  getCellsFromDom,
  start,
  stop
};
