require('../example');
const example = window.example;

// Test Hooks
beforeAll(() => {
    console.log('Before All');
})

afterAll(() => {
    console.log('After All');
})

beforeEach(() => {
    console.log('Before each test');
})

afterEach(() => {
    console.log('After each test');
})

//Test Bundles
describe('Assertion example', () => {
    test('should be defined', () => {
        expect(example).toBeDefined();
    });
    test('should be type of Object', () => {
        expect(typeof example).toEqual('object');
    });
})

//Simple Test
test('Function call result test', () => {
    const name = 'Mark'
    const expected = example.hello(name);
    expect(expected).toEqual('Hello Mark');
})