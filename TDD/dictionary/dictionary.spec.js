const dictionary = require('./dictionary');

describe('dictonary', () => {
    test('an empty string should return an empty object ', () => {
        expect(dictionary('')).toEqual({});
    });

    test('should count 1 for one word', () => {
        expect(dictionary('the')).toEqual({the : 1});
    })

    test('should count 2 for two equal words', () => {
        expect(dictionary('the the')).toEqual({the : 2});
    })

    test('should count 2 for two equal words and 1 for the different one', () => {
        expect(dictionary('the the request')).toEqual({the : 2, request: 1});
    })
})