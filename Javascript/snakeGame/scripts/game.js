let lifes = 3;
let score = 0;
let pearlPosX = 0;
let pearlPosY = 0;

window.onload = function() {
    startgame();
}

function drawPearl() {
    drawCircle(pearlPosX,pearlPosY,10,'white')
}

function generatePearl() {

    pearlPosX = Math.floor(board.width * Math.random());
    pearlPosY = Math.floor(board.height * Math.random());
   
}

function eatPearl() {
    updateScore(5);
    generatePearl();
}

function updateScore(value) {
    score += value;
    scoreBoard.textContent = score;

    if (score > 0) {
        if(score % 100 === 0) {
            playerSpeed = Math.min(15,playerSpeed+1);
        }
        if(score % 25 === 0) {
            startEnemy();
        }
    }
}

function drawLifes() {
    lifesBox.textContent = '';
    for (i = 0; i < lifes; i++) {
        lifesBox.textContent = lifesBox.textContent + '♥';
    }   
}

function looseLife() {
    if (lifes === 0) {
        gameOver();
        return;
    }
    lifes -= 1;
    drawLifes();
    startPlayer();
}

function moveEverything() {
    movePlayer();
    moveEnemies();
}

function drawBoard() {
    boardContext.fillStyle = '#323232';
    boardContext.fillRect(0,0,board.width, board.height)
}

function drawEverything() {
    drawBoard();
    drawPearl();
    drawPlayer();
    drawEnemies();  
}

function startgame() {
    drawLifes();
    updateScore(0);
    generatePearl();
    startPlayer();
    interval = setInterval(function() {
        moveEverything();
        drawEverything();
    }, 1000/FPS);
}

function gameOver() {
    clearInterval(interval);
}