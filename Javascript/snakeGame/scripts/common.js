const DIRECTION_RIGHT = 'ArrowRight';
const DIRECTION_LEFT = 'ArrowLeft';
const DIRECTION_TOP = 'ArrowUp';
const DIRECTION_DOWN = 'ArrowDown';
const SIZE = 10;
const FPS = 30;

const scoreBoard = document.getElementById('scoreValue');
const lifesBox = document.getElementById('lifes');
const board = document.getElementById('board');
const boardContext = board.getContext('2d');
let interval = null;

function drawCircle(x,y,r,color) {
    boardContext.fillStyle = color;
    boardContext.beginPath();
    boardContext.arc(x,y,r,0, Math.PI * 2);
    boardContext.fill();
    
}