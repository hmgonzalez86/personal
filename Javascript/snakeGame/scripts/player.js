let playerDirection;

let playerPosition = {
  x: 400,
  y: 300,
};

let playerSpeed = 5;

document.addEventListener("keydown", (event) => {
  switch (event.key) {
    case DIRECTION_RIGHT:
      playerDirection = DIRECTION_RIGHT;
      break;
    case DIRECTION_LEFT:
      playerDirection = DIRECTION_LEFT;
      break;
    case DIRECTION_TOP:
      playerDirection = DIRECTION_TOP;
      break;
    case DIRECTION_DOWN:
      playerDirection = DIRECTION_DOWN;
      break;
  }
});

function movePlayer() {
  switch (playerDirection) {
    case DIRECTION_RIGHT:
      playerPosition.x += playerSpeed;
      break;
    case DIRECTION_LEFT:
      playerPosition.x -= playerSpeed;
      break;
    case DIRECTION_TOP:
      playerPosition.y -= playerSpeed;
      break;
    case DIRECTION_DOWN:
      playerPosition.y += playerSpeed;
      break;
  }
  if (checkColision(playerPosition.x, playerPosition.y)) looseLife();
  if (checkPearl(playerPosition.x, playerPosition.y)) {
    eatPearl();
    generatePearl();
  }
}

function checkColision(x, y) {
  return (
    x > board.width ||
    y > board.height ||
    x < 0 ||
    y < 0 ||
    checkEnemies(x, y)
  );
}

function checkPearl(x, y) {
  return (
    x >= pearlPosX - SIZE &&
    x <= pearlPosX + SIZE &&
    y >= pearlPosY - SIZE &&
    y <= pearlPosY + SIZE
  ) 
}

function drawPlayer() {
  drawCircle(playerPosition.x, playerPosition.y, 10, "green");
}

function startPlayer() {
  playerPosition.x = 400;
  playerPosition.y = 300;
  const directionUpdate = Math.random();
  if (directionUpdate > 0.75) playerDirection = DIRECTION_TOP;
  if (directionUpdate > 0.5 && directionUpdate <= 0.75)
    playerDirection = DIRECTION_RIGHT;
  if (directionUpdate > 0.25 && directionUpdate <= 0.5)
    playerDirection = DIRECTION_LEFT;
  if (directionUpdate >= 0 && directionUpdate <= 0.25)
    playerDirection = DIRECTION_DOWN;
}
