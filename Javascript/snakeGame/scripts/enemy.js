class Enemy {
  position = { x: 0, y: 0 };
  speed = {
    x: 1,
    y: 1,
  };
  size = 10;

  constructor(x, y, sx, sy, size) {
    this.position.x = x;
    this.position.y = y;
    this.speed.x = sx;
    this.speed.y = sy;
    this.size = size;
  }

  moveEnemy() {
    this.position.x += this.speed.x;
    this.position.y += this.speed.y;

    if (this.position.x > board.width || this.position.x < 0) {
      this.speed.x *= -1;
    }

    if (this.position.y > board.height || this.position.y < 0) {
      this.speed.y *= -1;
    }
  }
}

const enemies = [];

function startEnemy() {
  enemies.push(
    new Enemy(
      Math.random() * board.width,
      Math.random() * board.height,
      Math.random() * 5,
      Math.random() * 5,
      (Math.random() * 10) + 10
    )
  );
}

function checkEnemies(x, y) {
  return enemies.some(enemy => 
      x >= enemy.position.x - enemy.size &&
      x <= enemy.position.x + enemy.size &&
      y >= enemy.position.y - enemy.size &&
      y <= enemy.position.y + enemy.size
  );
}

function moveEnemies() {
  enemies.forEach((enemy) => {
    enemy.moveEnemy();
  });
}

function drawEnemies() {
  enemies.forEach((enemy) => {
    this.drawEnemy(enemy.position.x, enemy.position.y, enemy.size);
  });
}

function drawEnemy(x, y, size) {
  drawCircle(x, y, size, "red");
}
