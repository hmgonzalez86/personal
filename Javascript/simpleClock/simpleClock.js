const hourNeedle = document.getElementById("hour");
const minutesNeedle = document.getElementById("minutes");
const secondsNeedle = document.getElementById("seconds");

setInterval(() => {

    const date = new Date();
    const hour = date.getHours() * 30;
    const minutes = date.getMinutes() * 6;
    const seconds = date.getSeconds() * 6;

    hourNeedle.style.transform = `rotateZ(${hour + (minutes/12)}deg)`;
    minutesNeedle.style.transform = `rotateZ(${minutes}deg)`;
    secondsNeedle.style.transform = `rotateZ(${seconds}deg)`;

}, 1000);